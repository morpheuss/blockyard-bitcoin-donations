<?php
defined( 'ABSPATH' ) || die;

/**
 * Class Blockyard_Bitcoin_User_Profile
 */
class Blockyard_Bitcoin_User_Profile {

	/**
	 * Blockyard_Bitcoin_User_Profile constructor.
	 *
	 * This is a "magic method" for PHP classes.
	 * The __construct() function is called whenever the class is instantiated.
	 *
	 * Classes are instantiated, "come to life" when created with a new command.
	 * Otherwise they are just code sitting in memory waiting to have a purpose in life.
	 */
	public function __construct() {

		// These are WordPress action hooks that run when editing or showing the user profile page.
		// You can find the details in the WordPress Function Reference (aka Codex) https://developer.wordpress.org/reference/hooks/show_user_profile/
		add_action( 'show_user_profile', array( $this , 'add_meta_fields' ) );
		add_action( 'edit_user_profile', array( $this , 'add_meta_fields' ) );

		// These WordPress action hooks fire when the profile is being updated
		// useful for saving data
		add_action( 'personal_options_update', array( $this , 'save_meta_data' ) );
		add_action( 'edit_user_profile_update', array( $this , 'save_meta_data' ) );

	}


	/**
	 * Add meta fields.
	 *
	 * This gets called with the show_user_profile or edit_user_profile WordPress
	 * action hooks.
	 *
	 * It receives the current user object for the user's profile being shown.
	 *
	 * @param WP_User $user
	 */
	public function add_meta_fields( $user ) {
		// This next closing PHP tag ends the PHP code and allows us to use HTML
		// right in the middle of our code.
		?>
		<h3>Blockyard Bitcoin Donations Info</h3>
		<table class="form-table">
			<tr>
				<th><label for="bitcoin">Bitcoin</label></th>
				<td>
					<input type="text" name="bitcoin" id="bitcoin" value="<?php echo esc_attr( get_the_author_meta( 'bitcoin', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description">Please enter your Bitcoin address.</span>
				</td>
			</tr>
		</table>
		<?php
		// We re-start the PHP code with an opening PHP tag
		// Why is the comment below?  Becuase // is NOT a valid HTML construct
		// remember we are in "HTML mode" until that <?php appears
		// We need to re-enter "PHP mode" to close our function and class.
	}

	/**
	 * Save the meta data.
	 *
	 * @param int $user_id
	 *
	 * @return bool
	 */
	public function save_meta_data( $user_id ) {
		if ( !current_user_can( 'edit_user', $user_id ) )
			return false;
		update_user_meta( $user_id, 'bitcoin', $_POST['bitcoin'] );
	}
}
